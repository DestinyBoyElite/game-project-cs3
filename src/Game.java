//region Imports
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
//endregion

public class Game {
    //region Instance Vars
    private ArrayList<Sprite> gameObjs;
    //endregion

    /**
     * The Game constructor instantiates the gameObjs ArrayList and creates a StdDraw canvas
     * @throws InterruptedException needed exception
     */
    public Game() throws InterruptedException {
        gameObjs = new ArrayList<>();
        StdDraw.setCanvasSize(800, 800);
        StdDraw.enableDoubleBuffering();
    }

    /**
     * The startMenu() method creates the title screen which includes animations and buttons
     * @throws InterruptedException needed exception
     * @throws IOException needed exception
     * @throws FontFormatException needed exception
     */
    public void startMenu() throws InterruptedException, IOException, FontFormatException {

        //region Sprites
        Sprite backGround = new Sprite(new Vector(), new String[]{"Backgrounds\\spaceGif.png\\frameOne.v2.png", "Backgrounds\\spaceGif.png\\frameTwo.v2.png", "Backgrounds\\spaceGif.png\\frameThree.v2.png"}); //Creates the Sprite for the stars background
        Sprite sun = new Sprite(new Vector(), new String[]{"Backgrounds\\sun.png\\f1.png", "Backgrounds\\sun.png\\f2.png","Backgrounds\\sun.png\\f3.png","Backgrounds\\sun.png\\f4.png","Backgrounds\\sun.png\\f5.png","Backgrounds\\sun.png\\f6.png","Backgrounds\\sun.png\\f7.png","Backgrounds\\sun.png\\f8.png","Backgrounds\\sun.png\\f9.png","Backgrounds\\sun.png\\f10.png","Backgrounds\\sun.png\\f11.png","Backgrounds\\sun.png\\f12.png","Backgrounds\\sun.png\\f13.png","Backgrounds\\sun.png\\f14.png"}); //Creates the Sprite for the sun
        //endregion

        //region Fonts
        StdDraw.setPenColor(Color.green); //Sets the pen color to grean
        Font planetFont = Font.createFont(Font.TRUETYPE_FONT, new File("Fonts\\xeliard\\Xeliard.ttf")).deriveFont(35f); //Creates the font for the buttons
        Font titleFont = Font.createFont(Font.TRUETYPE_FONT, new File("Fonts\\xeliard\\Xeliard.ttf")).deriveFont(50f); //Creates the font for the title of the game
        //endregion

        //region The Ship Object and All of the its Attributes
        int shipAnim = 0; //Integer to represent which ship animation will be played
        boolean anim = false; //Boolean to represent if an animation can run
        boolean startGame = false; //Boolean to represent if the "play" button is pressed
        double x = 0, y = 0, angle = 0; //Doubles to represent the x, y, and angle of the ship during the animations
        double playBtnScale = .15, wipBtnScale = .23, sunScale = .5; //Doubles to represent the scale of the objects
        double playBtnY = .2, wipBtnX = .83, sunY = 1; //Doubles to represent the pos of the objects
        MovableGameObject ship = new MovableGameObject(new Vector(), new Vector(0,0), new Vector(-.5,-.5), new String[]{"Ships\\ShipOne.png"}); //Creates a MovableGameObject to represent the ship
        //endregion

        while(true){

            //region Background Sprite Animation
            StdDraw.picture(.5,.5, backGround.nextFrame(), 1, 1); //Displays and animates the stars
            if(sunScale>.005) {
                StdDraw.picture(.5, sunY, sun.nextFrame(), sunScale, sunScale); //Displays and animates the sun
            }
            //endregion

            //region Dynamic Planet Animation Based on the Mouse
            if(playBtnScale>0.005) {
                if (Math.sqrt(Math.pow(StdDraw.mouseX() - .5, 2) + Math.pow(StdDraw.mouseY() - .2, 2)) <= .075) {
                    StdDraw.picture(.5, playBtnY, "Planets\\PlanetsLarge\\Barren2.v2.png", playBtnScale, playBtnScale);
                    if (StdDraw.isMousePressed()) {
                        startGame = true;
                    }
                } //Checks to see if the player is hovering the mouse pointer over the "play" button and changes the sprite

                else {
                    StdDraw.picture(.5, playBtnY, "Planets\\PlanetsLarge\\Barren.v2.png", playBtnScale, playBtnScale);
                } //Uses default sprite for the "play" button if the player is not hovering the mouse pointer over the "play" button
            }
            if(wipBtnScale>0.005) {
                if (Math.sqrt(Math.pow(StdDraw.mouseX() - .83, 2) + Math.pow(StdDraw.mouseY() - .53, 2)) <= .115) {
                    StdDraw.picture(wipBtnX, .53, "Planets\\PlanetsLarge\\Ice2.v2.png", wipBtnScale, wipBtnScale);
                } //Checks to see if the player is hovering the mouse pointer over the alternate button and changes the sprite

                else {
                    StdDraw.picture(wipBtnX, .53, "Planets\\PlanetsLarge\\Ice.v2.png", wipBtnScale, wipBtnScale);
                } //Uses default sprite for the alternate button if the player is not hovering the mouse pointer over the alternate button
            }
            //endregion

            //region Creates the Title Text
            if(!startGame) {
                StdDraw.setFont(titleFont); //Sets the current font to titleFont
                StdDraw.text(.3, .6, "Into the Galaxy", 30); //Displays the title
                StdDraw.line(.1, .453, .53, .69); //Underlines the title
            }
            //endregion

            //region Ship Animation
            if((ship.getPos().getX()<=-.5 || ship.getPos().getX()>1.5) && (ship.getPos().getY()<=-.5 || ship.getPos().getY()>1.5)){
                double rand = Math.random(); //Double to pick a random ship animation

                if(rand < .33){
                    x = 0;
                    y = -.5;
                    shipAnim = 0;
                    angle = 80;
                } //Sets up the variables for ship animation 1

                else if(rand < .66){
                    x = -.5;
                    y = 0;
                    shipAnim = 1;
                    angle = -30;
                }//Sets up the variables for ship animation 2

                else{
                    x = 0;
                    y = 0;
                    shipAnim = 2;
                    angle = -90;
                } //Sets up the variables for ship animation 3

                ship.setPos(new Vector(-.2, -.2));//Puts the ship off of the screen so that it can wait for the next animation

                anim = false; //Sets the anim variable to false so that only one animation can run at a time
            } //Picks which ship animation will happen

            double rand = Math.random(); //Double to determine, at random, if an animation will occur
            if(rand < .1){
                anim = true; //Sets the anim variable to true so that an animation can occur
            } //10% chance of there being a ship animation

            if(!startGame) {
                if (anim) {
                    if (shipAnim == 0) {
                        ship.getPos().setHeading(Math.toDegrees(Math.tan(((((y += .006) + .5) - ship.getPos().getY()) / (((2 * Math.pow(y, 2)) + .6) - ship.getPos().getX()))))); //Calculates the current heading of the ship during the animation
                        StdDraw.picture(ship.getPos().getX(), ship.getPos().getY(), ship.move(), .1, .1, angle -= 1.6); //Displays the ship
                        ship.setPos(new Vector((2 * Math.pow(y, 2)) + .6, (y += .005) + .5)); //Updates the ship's position
                    } //Runs ship animation 1

                    else if (shipAnim == 1) {
                        ship.getPos().setHeading(Math.toDegrees(Math.tan(((((-Math.pow(x, 2)) + .4) - ship.getPos().getY()) / (((x += .01) + .5) - ship.getPos().getX()))))); //Calculates the current heading of the ship during the animation
                        StdDraw.picture(ship.getPos().getX(), ship.getPos().getY(), ship.move(), .1, .1, angle -= 2); //Displays the ship
                        ship.setPos(new Vector((x += .01) + .5, (-Math.pow(x, 2)) + .4)); //Updates the ship's position
                    } //Runs ship animation 2

                    else if (shipAnim == 2) {
                        ship.getPos().setHeading(Math.toDegrees(Math.tan(((((Math.pow(x, 2) / 2) + .5) - ship.getPos().getY()) / (((x += .01)) - ship.getPos().getX()))))); //Calculates the current heading of the ship during the animation
                        StdDraw.picture(ship.getPos().getX(), ship.getPos().getY(), ship.move(), .1, .1, angle += .7); //Displays the ship
                        ship.setPos(new Vector((x += .005), (Math.pow(x, 2) / 2) + .5)); //Updates the ship's position
                    } //Runs ship animation 3
                } //Runs the ship animation
            }
            //endregion

            //region Creates Button Text
            if(!startGame) {
                StdDraw.setFont(planetFont); //Sets the current font to planetFont
                StdDraw.text(.5, .2, "PLAY"); //Displays the "play" button on the barren planet
                StdDraw.text(.83, .53, "WIP"); //Displays the alternate button on the ice planet
            }
            //endregion

            //region Play Button Clicked
            if(startGame){
                if(sunScale-.005>0) {
                    sunScale -= .005;
                }
                if(wipBtnScale-.005>0) {
                    wipBtnScale -= .005;
                }
                if(playBtnScale-.005>0) {
                    playBtnScale -= .005;
                }
                if(sunY>.5){
                    sunY-=.005;
                }
                if(wipBtnX>.5){
                    wipBtnX-=.007;
                }
                if(playBtnY<.5){
                    playBtnY+=.008;
                }
            }
            //endregion

            //region Default Displaying
            StdDraw.show(); //Displays the StdDraw canvas
            Thread.sleep(30); //Delays the code so that it does not animate too fast
            //endregion

        } //Infinite loop to run all of the animations for the title screen
    }

    public void startGame(){

    }
}