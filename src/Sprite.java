//region Imports
import java.util.ArrayList;
import java.util.Arrays;
//endregion

public class Sprite {

    //region Instance Vars
    private ArrayList<String> images;
    private Vector pos;
    private int currFrame;
    //endregion

    /**
     * The Sprite default constructor sets default value to all of the instance vars
     */
    public Sprite(){
        images = new ArrayList<>();
        pos = null;
        currFrame = 0;
    }

    /**
     * The Sprite constructor sets the position vector to the param p and the images ArrayList to i
     * @param p Vector to represent the Position
     * @param i String[] to represent all of the image file paths
     */
    public Sprite(Vector p, String[] i){
        pos = p;
        images = new ArrayList<>(Arrays.asList(i));
        currFrame = 0;
    }

    /**
     * The nextFrame() method displays the next image in the images ArrayList so that it can be animated
     * @return String representing the file path to the current image
     */
    public String nextFrame(){
        return images.get(currFrame=(currFrame+1)%images.size());
    }

    /**
     * The getCurrFrame() method returns the current image that is to be displayed
     * @return String representing the file path to the current image
     */
    public String getCurrFrame(){
        return images.get(currFrame);
    }

    //region Gets and Sets
    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public Vector getPos() {
        return pos;
    }

    public void setPos(Vector pos) {
        this.pos = pos;
    }
    //endregion
}