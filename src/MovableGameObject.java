public class MovableGameObject extends Sprite{

    //region Instance Vars
    private Vector acc;
    private Vector vel;
    //endregion

    /**
     * The MovableGameObject default constructor sets default value to the instance vars
     */
    public MovableGameObject(){
        acc = new Vector();
        vel = new Vector();
    }

    /**
     * The MovableGameObject() constructor sets the instance vars to their corresponding params
     * @param acc Vector to represent the Acceleration
     * @param vel Vector to represent the Velocity
     * @param pos Vector to represent the Position
     * @param i String[] to represent all of the image file paths
     */
    public MovableGameObject(Vector acc, Vector vel, Vector pos, String[] i) {
        super(pos, i);
        this.acc = acc;
        this.vel = vel;
    }

    /**
     * The move() method updates the sprites position by velocity
     * @return String representing the file path to the current image
     */
    public String move(){
        getPos().update(vel);
        return getCurrFrame();
    }

    //region Gets and Sets
    public Vector getAcc() {
        return acc;
    }

    public void setAcc(Vector acc) {
        this.acc = acc;
    }

    public Vector getVel() {
        return vel;
    }

    public void setVel(Vector vel) {
        this.vel = vel;
    }
    //endregion
}