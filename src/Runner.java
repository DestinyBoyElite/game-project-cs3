//region Imports
import java.awt.*;
import java.io.IOException;
//endregion

public class Runner {
    public static void main(String[] args) throws InterruptedException, IOException, FontFormatException {
        Game one = new Game();
        one.startMenu();
    }
}